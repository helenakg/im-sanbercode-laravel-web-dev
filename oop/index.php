<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


//SHEEP
$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun" 
echo "legs : " . $sheep->legs . "<br>"; // 4
echo "cold blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

//FROG
$kodok = new Frog("buduk");

echo "Name : " . $kodok->name . "<br>";
echo "legs : " . $kodok->legs . "<br>";
echo "cold blooded : " . $kodok->cold_blooded . "<br>";
echo $kodok->jump("Hop Hop"); // "hop hop"

//APE
$sungokong = new Ape("kera sakti");

echo "Name : " . $sungokong->name . "<br>";
echo "legs : " . $sungokong->legs . "<br>";
echo "cold blooded : " . $sungokong->cold_blooded . "<br>";
echo $sungokong->yell("Auooo"); // "Auooo"

?>