<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'home']);
Route::get('/register', [AuthController::class,'register']);
Route::post('/welcome', [AuthController::class,'welcome']);

Route::get('/table', function(){
    return view('pages.table');
});

Route::get('/data-table', function(){
    return view('pages.data-tables');
});

//CRUD

//Create Data
//Route mengarah ke form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);
//Route untuk menyimpan inputan ke dalam database cast
Route::post('/cast', [CastController::class, 'store']);

//Read Data
//Route mengarah ke halaman tampil semua data di tabel cast
Route::get('/cast', [CastController::class, 'index']);
//Route detail kategori berdasarkan id
Route::get('/cast/(id)', [CastController::class, 'show']);
