@extends('layouts.master')

@section('title')
Halaman Show Cast
    
@endsection
  
@section('sub-title')
Cast
@endsection

@section('content')
<a href="/cast/create" class=btn btn-primary btn-sm my-2>Tambah Cast</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
           <tr>
                <td>{{$key + 1}}</td>    
                <td>{{$item->nama}}</td>
                <td>
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm" >Detail</a>    
                </td>    
        </tr> 
        @empty
            <tr>
                <td>Data Cast Kosong</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection