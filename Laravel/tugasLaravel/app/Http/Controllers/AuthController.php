<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('pages.register');
    }

    public function welcome(request $request)
    {
        //dd($request->all());
        $fname = $request['firstname'];
        $lname = $request['lastname'];
        $nat = $request['nationality'];
        $bio = $request['bio'];

        return view ('pages.welcome', ['fname' => $fname, 'lname' => $lname]);

    }
    
}
